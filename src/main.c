/*
 * Copyright (c) 2020 Nordic Semiconductor ASA
 *
 * SPDX-License-Identifier: LicenseRef-Nordic-5-Clause
 */

/** @file
 *  @brief Central Heart Rate over LE Coded PHY sample
 */

#include <zephyr/types.h>
#include <stddef.h>
#include <errno.h>
#include <zephyr.h>
#include <sys/byteorder.h>
#include <logging/log.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>
#include <bluetooth/gatt_dm.h>
#include <bluetooth/scan.h>

#include <bluetooth/hci_vs.h>
#include <sys/byteorder.h>

#include "gpio_util.h"

#define     CONN_INTERVAL_MIN   12
#define     CONN_INTERVAL_MAX   14
#define     SLAVE_LATENCY       4
#define     CONN_SUPER_TO       225

LOG_MODULE_REGISTER(CentralHrLR, CONFIG_LOG_MAX_LEVEL);
#define sd(x)   log_strdup((x))

static struct bt_conn *default_conn;

static struct bt_gatt_subscribe_params subscribe_params;

static uint8_t notify_func(struct bt_conn *conn,
		struct bt_gatt_subscribe_params *params,
		const void *data, uint16_t length)
{
	if (!data) {
		LOG_DBG("[UNSUBSCRIBED]");
		params->value_handle = 0U;
                set_led_state(0, 1);
                enable_blink_led(0, 500);
		return BT_GATT_ITER_STOP;
	}

	if (length == 2) {
		uint8_t hr_bpm = ((uint8_t *)data)[1];

		LOG_DBG("[NOTIFICATION] Heart Rate %u bpm", hr_bpm);
	} else {
		LOG_DBG("[NOTIFICATION] data %p length %u", data, length);
	}

	return BT_GATT_ITER_CONTINUE;
}

static void discover_hrs_completed(struct bt_gatt_dm *dm, void *ctx)
{
	int err;

	const struct bt_gatt_dm_attr *gatt_chrc;
	const struct bt_gatt_dm_attr *gatt_desc;

	struct bt_conn *conn = bt_gatt_dm_conn_get(dm);

	LOG_DBG("The discovery procedure succeeded");

	bt_gatt_dm_data_print(dm);

	/* Heart rate characteristic */
	gatt_chrc = bt_gatt_dm_char_by_uuid(dm, BT_UUID_HRS_MEASUREMENT);
	if (!gatt_chrc) {
		LOG_ERR("No heart rate measurement characteristic found");
		return;
	}

	gatt_desc = bt_gatt_dm_desc_by_uuid(dm, gatt_chrc,
			BT_UUID_HRS_MEASUREMENT);
	if (!gatt_desc) {
		LOG_ERR("No heat rate measurement characteristic value found");
		return;
	}

	subscribe_params.value_handle = gatt_desc->handle;

	gatt_desc = bt_gatt_dm_desc_by_uuid(dm, gatt_chrc,
			BT_UUID_GATT_CCC);

	if (!gatt_desc) {
		LOG_ERR("No heart rate CCC descriptor found. "
		       "Heart rate service does not support notifications.");
		return;
	}

	subscribe_params.notify = notify_func;
	subscribe_params.value = BT_GATT_CCC_NOTIFY;
	subscribe_params.ccc_handle = gatt_desc->handle;

	err = bt_gatt_subscribe(conn, &subscribe_params);
	if (err && err != -EALREADY) {
		LOG_ERR("Subscribe failed (err %d)\n", err);
	} else {
		LOG_DBG("[SUBSCRIBED]");
                disable_blink_led(0);
                set_led_state(0, 0);
	}

	err = bt_gatt_dm_data_release(dm);
	if (err) {
		LOG_ERR("Could not release the discovery data (err %d)\n", err);
	}
}

static void discover_hrs_service_not_found(struct bt_conn *conn, void *ctx)
{
	LOG_DBG("No more services");
}

static void discover_hrs_error_found(struct bt_conn *conn, int err, void *ctx)
{
	LOG_ERR("The discovery procedure failed, err %d\n", err);
}


static struct bt_gatt_dm_cb discover_hrs_cb = {
	.completed = discover_hrs_completed,
	.service_not_found = discover_hrs_service_not_found,
	.error_found = discover_hrs_error_found,
};



static void scan_filter_match(struct bt_scan_device_info *device_info,
			      struct bt_scan_filter_match *filter_match,
			      bool connectable)
{
	int err;
	char addr[BT_ADDR_LE_STR_LEN];
	struct bt_conn_le_create_param *conn_params;

	bt_addr_le_to_str(device_info->recv_info->addr, addr, sizeof(addr));

	LOG_DBG("Filters matched. Address: %s connectable: %s rssi: %d",
		sd(addr), connectable ? "yes" : "no", device_info->recv_info->rssi);

	err = bt_scan_stop();
	if (err) {
		LOG_ERR("Stop LE scan failed (err %d)\n", err);
	}

	conn_params = BT_CONN_LE_CREATE_PARAM(
			BT_CONN_LE_OPT_CODED | BT_CONN_LE_OPT_NO_1M,
			BT_GAP_SCAN_FAST_INTERVAL,
			BT_GAP_SCAN_FAST_INTERVAL);

        struct bt_le_conn_param my_params = {
            .interval_min = CONN_INTERVAL_MIN,
            .interval_max = CONN_INTERVAL_MAX,
            .latency = SLAVE_LATENCY,
            .timeout = CONN_SUPER_TO
        };
	err = bt_conn_le_create(device_info->recv_info->addr, conn_params,
				&my_params,
				&default_conn);

	if (err) {
		LOG_ERR("Create conn failed (err %d)\n", err);

		err = bt_scan_start(BT_SCAN_TYPE_SCAN_ACTIVE);
		if (err) {
			LOG_ERR("Scanning failed to start (err %d)\n", err);
			return;
		}
	}

	LOG_DBG("Connection pending");
}

BT_SCAN_CB_INIT(scan_cb, scan_filter_match, NULL, NULL, NULL);

static void scan_init(void)
{
	int err;

	/* Use active scanning and disable duplicate filtering to handle any
	 * devices that might update their advertising data at runtime. */
	struct bt_le_scan_param scan_param = {
		.type     = BT_LE_SCAN_TYPE_ACTIVE,
		.interval = BT_GAP_SCAN_FAST_INTERVAL,
		.window   = BT_GAP_SCAN_FAST_WINDOW,
		.options  = BT_LE_SCAN_OPT_CODED | BT_LE_SCAN_OPT_NO_1M
	};

	struct bt_scan_init_param scan_init = {
		.connect_if_match = 0,
		.scan_param = &scan_param,
		.conn_param = NULL
	};

	bt_scan_init(&scan_init);
	bt_scan_cb_register(&scan_cb);

	err = bt_scan_filter_add(BT_SCAN_FILTER_TYPE_UUID, BT_UUID_HRS);
	if (err) {
		LOG_ERR("Scanning filters cannot be set (err %d)\n", err);

		return;
	}

	err = bt_scan_filter_enable(BT_SCAN_UUID_FILTER, false);
	if (err) {
		LOG_ERR("Filters cannot be turned on (err %d)\n", err);
	}
}

static void connected_cb(struct bt_conn *conn, uint8_t conn_err)
{
	int err;
	struct bt_conn_info info;
	char addr[BT_ADDR_LE_STR_LEN];

	bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));

	if (conn_err) {
		LOG_ERR("Failed to connect to %s (%u)\n", sd(addr), conn_err);

		bt_conn_unref(default_conn);
		default_conn = NULL;

		err = bt_scan_start(BT_SCAN_TYPE_SCAN_ACTIVE);
		if (err) {
			LOG_ERR("Scanning failed to start (err %d)\n", err);
		}

		return;
	}

	err = bt_conn_get_info(conn, &info);

	if (err) {
		LOG_ERR("Failed to get connection info\n");
	} else {
		const struct bt_conn_le_phy_info *phy_info;

		phy_info = info.le.phy;
		LOG_DBG("Connected: %s, tx_phy %u, rx_phy %u\n",
				sd(addr), phy_info->tx_phy, phy_info->rx_phy);
	}

	if (conn == default_conn) {

		err = bt_gatt_dm_start(conn, BT_UUID_HRS, &discover_hrs_cb, NULL);
		if (err) {
			LOG_ERR("Failed to start discovery (err %d)\n", err);
		}
	}
}

static void disconnected_cb(struct bt_conn *conn, uint8_t reason)
{
	int err;
	char addr[BT_ADDR_LE_STR_LEN];

	bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));

	LOG_DBG("Disconnected: %s (reason 0x%02x)\n", sd(addr), reason);

	if (default_conn != conn) {
		return;
	}

	bt_conn_unref(default_conn);
	default_conn = NULL;

	err = bt_scan_start(BT_SCAN_TYPE_SCAN_ACTIVE);
	if (err) {
		LOG_ERR("Scanning failed to start (err %d)\n", err);
	}
}

static bool accept_update = false;
static bool le_param_req_cb(struct bt_conn *conn, struct bt_le_conn_param *param) {
    LOG_DBG("Connection parameter update request:\n\
             Connection Interval (1.25ms steps): %u - %u\n\
             Slave latency                     : %u\n\
             Supervision timeout (10ms steps)  : %u", param->interval_min, param->interval_max, param->latency, param->timeout);
    if (!accept_update) {
        LOG_WRN("Deny Peripheral connection renegotation. Default to Central parameters");
        LOG_DBG("Central connection setting:\n\
                 Connection Interval (1.25ms steps): %u - %u\n\
                 Slave latency                     : %u\n\
                 Supervision timeout (10ms steps)  : %u", CONN_INTERVAL_MIN, CONN_INTERVAL_MAX, SLAVE_LATENCY, CONN_SUPER_TO);
    }
    return accept_update;
}

static void le_param_updated_cb(struct bt_conn *conn, uint16_t interval, uint16_t latency, uint16_t timeout) {
    LOG_DBG("Connection parameters updated:\n\
             Connection Interval (1.25ms steps): %u\n\
             Slave latency                     : %u\n\
             Supervision timeout (10ms steps)  : %u", interval, latency, timeout);
}

static void le_phy_updated_cb(struct bt_conn *conn, struct bt_conn_le_phy_info *param) {
    LOG_DBG("Physcial layer updated:\n\
             RX Phy: %u\n\
             TX Phy: %u\n", param->rx_phy, param->tx_phy);
}

static struct bt_conn_cb conn_callbacks = {
	.connected = connected_cb,
	.disconnected = disconnected_cb,
        .le_param_req = le_param_req_cb,
        .le_param_updated = le_param_updated_cb,
        .le_phy_updated = le_phy_updated_cb
};

static void get_tx_power(int8_t *tx_pwr_lvl, uint8_t handle_type) {
    struct bt_hci_cp_vs_read_tx_power_level *cp;
    struct bt_hci_rp_vs_read_tx_power_level *rp;
    struct net_buf *buf, *rsp = NULL;
    uint16_t handle = 0;
    int err;

    *tx_pwr_lvl = 0xFF;
    buf = bt_hci_cmd_create(BT_HCI_OP_VS_READ_TX_POWER_LEVEL, sizeof(*cp));
    if (!buf) {
        LOG_ERR("Unable to allocate command buffer");
        return;
    }

    cp = net_buf_add(buf, sizeof(*cp));
    cp->handle = sys_cpu_to_le16(handle);
    cp->handle_type = handle_type;

    err = bt_hci_cmd_send_sync(BT_HCI_OP_VS_READ_TX_POWER_LEVEL, buf, &rsp);
    if (err) {
        uint8_t reason = rsp ? ((struct bt_hci_rp_vs_read_tx_power_level *)rsp->data)->status : 0;
        LOG_ERR("Read Tx power err: %d reason 0x%02x", err, reason);
        return;
    }

    rp = (void *)rsp->data;
    *tx_pwr_lvl = rp->tx_power_level;

    net_buf_unref(rsp);
}

static int set_tx_power(int8_t tx_pwr_lvl, uint8_t handle_type) {
    struct bt_hci_cp_vs_write_tx_power_level *cp;
    struct bt_hci_rp_vs_write_tx_power_level *rp;
    struct net_buf *buf, *rsp = NULL;
    uint16_t handle = 0;
    int err;

    int8_t default_power;
    int8_t updated_power;
    get_tx_power(&default_power, handle_type);
  
    buf = bt_hci_cmd_create(BT_HCI_OP_VS_WRITE_TX_POWER_LEVEL, sizeof(*cp));
    if (!buf) {
        LOG_ERR("Unable to allocate command buffer");
        return 1;
    }
  
    cp = net_buf_add(buf, sizeof(*cp));
    cp->handle = sys_cpu_to_le16(handle);
    cp->handle_type = handle_type;
    cp->tx_power_level = tx_pwr_lvl;
  
    err = bt_hci_cmd_send_sync(BT_HCI_OP_VS_WRITE_TX_POWER_LEVEL, buf, &rsp);
    if (err) {
        uint8_t reason = rsp ?
            ((struct bt_hci_rp_vs_write_tx_power_level *)
              rsp->data)->status : 0;
        LOG_ERR("Set Tx power err: %d reason 0x%02x", err, reason);
        return err;
    }
  
    rp = (void *)rsp->data;
    LOG_DBG("Response BLE Tx Power: %d", rp->selected_tx_power);

    get_tx_power(&updated_power, handle_type);

    if (handle_type == BT_HCI_VS_LL_HANDLE_TYPE_ADV) {
        LOG_DBG("Advertising power modified : %d -> %d", default_power, updated_power);
    } else if (handle_type == BT_HCI_VS_LL_HANDLE_TYPE_CONN) {
        LOG_DBG("Connection power modified  : %d -> %d", default_power, updated_power);
    } else if (handle_type == BT_HCI_VS_LL_HANDLE_TYPE_SCAN) {
        LOG_DBG("Scan power modified  : %d -> %d", default_power, updated_power);
    }

    net_buf_unref(rsp);
    return 0;
}



void main(void)
{
        LOG_DBG("Start Central HRM Test - v0.2");
	int err;

        init_gpios();

	err = bt_enable(NULL);
	if (err) {
		LOG_ERR("Bluetooth init failed (err %d)\n", err);
		return;
	}

	LOG_DBG("Bluetooth initialized");
        k_sleep(K_SECONDS(1));

        int8_t power_lvl = 8;
        uint8_t handle_type = BT_HCI_VS_LL_HANDLE_TYPE_SCAN;
        err = set_tx_power(power_lvl, handle_type);
        if (err) {
            LOG_ERR("Error setting Scan TX power\n");
        } else {
            LOG_DBG("Scan Power set");
        }

        handle_type = BT_HCI_VS_LL_HANDLE_TYPE_ADV;
        err = set_tx_power(power_lvl, handle_type);
        if (err) {
            LOG_ERR("Error setting Advertising TX power\n");
        } else {
            LOG_DBG("Advertising Power set");
        }

	bt_conn_cb_register(&conn_callbacks);

	scan_init();

        k_sleep(K_SECONDS(1));

	err = bt_scan_start(BT_SCAN_TYPE_SCAN_ACTIVE);
	if (err) {
		LOG_ERR("Scanning failed to start (err %d)\n", err);
		return;
	}
        enable_blink_led(0, 500);

	LOG_DBG("Scanning successfully started");
}
