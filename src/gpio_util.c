/** @file
 * @defgroup ble_peripheral_gpio_util gpio_util.c
 * @{
 * @ingroup ble_peripheral
 * @brief GPIO Utilities
 *
 * Configures GPIOs to be used as interrupts on the device. Also allows
 * caller to read and set (output only GPIOs) the state of GPIOs.
 */
#include "gpio_util.h"
#include <device.h>
#include <drivers/gpio.h>
#include "logging/log.h"

/** @brief Declare Logger */
LOG_MODULE_DECLARE(CentralHrLR, CONFIG_LOG_MAX_LEVEL);

#define LED0_NODE           DT_ALIAS(led0)
#define LED1_NODE           DT_ALIAS(led1)
#define LED2_NODE           DT_ALIAS(led2)
#define LED3_NODE           DT_ALIAS(led3)

#if DT_NODE_HAS_STATUS(LED0_NODE, okay)
#define LED0                DT_GPIO_LABEL(LED0_NODE, gpios)
#define PIN0                DT_GPIO_PIN(LED0_NODE, gpios)
#define FLAGS0              DT_GPIO_FLAGS(LED0_NODE, gpios)
#else
#error "Unsupported board: led0 devicetree alias is not defined"
#define LED0                ""
#define PIN0                0
#define FLAGS0              0
#endif

#if DT_NODE_HAS_STATUS(LED1_NODE, okay)
#define LED1                DT_GPIO_LABEL(LED1_NODE, gpios)
#define PIN1                DT_GPIO_PIN(LED1_NODE, gpios)
#define FLAGS1              DT_GPIO_FLAGS(LED1_NODE, gpios)
#else
#error "Unsupported board: led1 devicetree alias is not defined"
#define LED1                ""
#define PIN1                0
#define FLAGS1              0
#endif

#if DT_NODE_HAS_STATUS(LED2_NODE, okay)
#define LED2                DT_GPIO_LABEL(LED2_NODE, gpios)
#define PIN2                DT_GPIO_PIN(LED2_NODE, gpios)
#define FLAGS2              DT_GPIO_FLAGS(LED2_NODE, gpios)
#else
#error "Unsupported board: led2 devicetree alias is not defined"
#define LED2                ""
#define PIN2                0
#define FLAGS2              0
#endif

#if DT_NODE_HAS_STATUS(LED3_NODE, okay)
#define LED3                DT_GPIO_LABEL(LED3_NODE, gpios)
#define PIN3                DT_GPIO_PIN(LED3_NODE, gpios)
#define FLAGS3              DT_GPIO_FLAGS(LED3_NODE, gpios)
#else
#error "Unsupported board: led3 devicetree alias is not defined"
#define LED3                ""
#define PIN3                0
#define FLAGS3              0
#endif

#define NUM_LEDS 4
static int m_led_index[NUM_LEDS] = { 0, 1, 2, 3 };
static int m_led_status_array[NUM_LEDS] = { 0, 0, 0, 0 };

static const char *m_led_name_array[NUM_LEDS] = { LED0, LED1, LED2, LED3 };
static gpio_pin_t m_pin_array[NUM_LEDS] = { PIN0, PIN1, PIN2, PIN3 };
static int m_flag_array[NUM_LEDS] = { GPIO_OUTPUT_HIGH, GPIO_OUTPUT_HIGH, GPIO_OUTPUT_HIGH, GPIO_OUTPUT_HIGH };

static void led_timer_handler(struct k_timer *dummy) {
    int led_index = *((int *)(dummy->user_data));
    //LOG_DBG("Led: %d set to %d", led_index, m_led_status_array[led_index]);
    set_led_state(led_index, m_led_status_array[led_index]);
    m_led_status_array[led_index] = !m_led_status_array[led_index];
}
K_TIMER_DEFINE(m_led_0_timer, led_timer_handler, NULL);
K_TIMER_DEFINE(m_led_1_timer, led_timer_handler, NULL);
K_TIMER_DEFINE(m_led_2_timer, led_timer_handler, NULL);
K_TIMER_DEFINE(m_led_3_timer, led_timer_handler, NULL);
static struct k_timer *m_timer_list[NUM_LEDS] = { &m_led_0_timer, &m_led_1_timer, &m_led_2_timer, &m_led_3_timer };

static bool is_led_supported(int led) {
    return (m_pin_array[led] == 0 ? false : true);
}

int init_gpios(void) {
    LOG_DBG("Initializing GPIOs...");
    int err = -1;
    const struct device *dev;

    for (int i = 0; i < NUM_LEDS; i++) {
        if (!is_led_supported(i)) {
            LOG_WRN("LED %d not supported on this board", i);
            break;
        }
        dev = device_get_binding(m_led_name_array[i]);
        if (dev != NULL) {
            err = gpio_pin_configure(dev, m_pin_array[i], m_flag_array[i]);
            if (err) {
                LOG_ERR("Error configuring LED %d", i);
                break;
            }
        }
    }
    return err;
}

int get_led_state(int led) {
    if (led < 0 || led > 3) {
        LOG_ERR("Invalid LED: %d", led);
        return -1;
    }
    if (!is_led_supported(led)) {
        LOG_ERR("LED %d not supported on this board", led);
        return -1;
    }

    const struct device *dev = device_get_binding(m_led_name_array[led]);
    if (dev != NULL) {
        return gpio_pin_get(dev, m_pin_array[led]);
    }
    return -1;
}

void set_led_state(int led, int state) {
    if (led < 0 || led > 3) {
        LOG_ERR("Set Led: Invalid LED: %d", led);
        return;
    }
    if (!is_led_supported(led)) {
        LOG_ERR("LED %d not supported on this board", led);
        return;
    }

    const struct device *dev = device_get_binding(m_led_name_array[led]);
    if (dev != NULL) {
        gpio_pin_set(dev, m_pin_array[led], state);
    }
}

/** @brief Enable Blinking an LED
 *
 * @details Function will blink the given LED.
 *
 * @param[in] led LED number (0 - 3)
 * @param[in] duration time in ms between LED blinks
 */
void enable_blink_led(int led, uint16_t period) {
    if (led < 0 || led > 3) {
        LOG_ERR("Blink LED: Invalid LED: %d", led);
        return;
    }
    if (!is_led_supported(led)) {
        LOG_ERR("LED %d not supported on this board", led);
        return;
    }
    m_timer_list[led]->user_data = &m_led_index[led];
    k_timer_start(m_timer_list[led], K_MSEC(period), K_MSEC(period));
}

/** @brief Disable Blinking an LED
 *
 * @details Function will stop blinking the given LED.
 *
 * @param[in] led LED number (0 - 3)
 */
void disable_blink_led(int led) {
    if (led < 0 || led > 3) {
        LOG_ERR("Blink Off: Invalid LED: %d", led);
        return;
    }
    if (!is_led_supported(led)) {
        LOG_ERR("LED %d not supported on this board", led);
        return;
    }
    k_timer_stop(m_timer_list[led]);
    set_led_state(led, 1);
}

/**
 * @}
 */
