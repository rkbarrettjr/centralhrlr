#ifndef GPIO_UTIL_H
#define GPIO_UTIL_H

#include <stdint.h>

int init_gpios(void);
int get_led_state(int led);
void set_led_state(int led, int state);
void enable_blink_led(int led, uint16_t period);
void disable_blink_led(int led);

#endif
