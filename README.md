# Central Heart Rate Monitor #

Repo contains firmware for the nRF52 series of development kits

### What does this firmware do? ###

Scans for peripherals operating as HRMs and connects to them.

### Repo and Target Setup ###

* Target: nRF52832-DK, nRf52840-DK, nRf52840-Dongle
* Project created using Segger Embedded Studio
* NCS / Zephyr

### Who do I talk to? ###

* Kent Barrett
* kbarrett@boldtype.com
